const React = require('react-native');
const {StyleSheet} =React;

export default {
    container: {
      flex: 1
    },/*
    row: {
      flexDirection: 'row',
      flex: 1,
      justifyContent: 'space-around',
      alignItems: 'center'
    },*/
    resultText: {
      fontSize: 30,
      color: 'white'
    },
    btn: {
      alignItems: 'center',
      alignSelf: 'stretch',
      justifyContent: 'center'
    },
    btnText: {
      fontSize: 35
    },
    calculationText: {
      fontSize: 42,
      color: 'white'
    },
    result: {
      flex: 2,
      backgroundColor: '#1C1C1C',
      justifyContent: 'center',
      alignItems: 'flex-end'
    },
    calculation: {
      flex: 1,
      backgroundColor: '#1C1C1C',
      justifyContent: 'center',
      alignItems: 'flex-end'
    },
    buttons: {
      flex: 7,
      flexDirection: 'row'
    },
    numbers: {
      flex: 3,
      backgroundColor: '#505050',
      flexDirection: 'row',
      flexWrap: 'wrap'
    },
    landscapeOperations:{
      flex: 3,
      backgroundColor: '#333333',
      flexDirection: 'row',
      flexWrap: 'wrap'
    },
    operations: {
      flex: 1,
      justifyContent: 'space-around',
      alignItems: 'stretch',
      backgroundColor: '#FF9500'
    },
    white: {
      color: 'white'
    }
}
