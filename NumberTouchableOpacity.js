import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Text, TouchableOpacity} from 'react-native';
import styles from './styles'

class NumberTouchableOpacity extends Component {
    static propTypes = {
        content: PropTypes.string.isRequired,
        color: PropTypes.string.isRequired,
        width: PropTypes.string.isRequired,
        height: PropTypes.string.isRequired
    }

    render() {
        const {content, color, width, height, onBtnPress } = this.props;

        return (
            <TouchableOpacity style={[styles.btn,{
                width: width,
                height: height,
                backgroundColor: color}]}
                onPress={()=>onBtnPress({content})}>
                <Text style={[styles.btnText, styles.white]} >{content}</Text>
            </TouchableOpacity>
        );
    }
}

export default NumberTouchableOpacity;
