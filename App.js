import React, { Component, useEffect } from 'react';
import SplashScreen from 'react-native-splash-screen';
import {
  Dimensions,
  StyleSheet,
  Text,
  View,
  TouchableOpacity

} from 'react-native';

import styles from './styles';
import NumberTouchableOpacity from './NumberTouchableOpacity';
import OperationTouchableOpacity from './OperationTouchableOpacity';

var mexp = require('math-expression-evaluator');

const portraitNumbers = [
  {content: '1', color: '#505050', width: '33.33%', height: '25%'},
  {content: '2',color: '#505050', width: '33.33%', height: '25%'},
  {content: '3',color: '#505050', width: '33.33%', height: '25%'},
  {content: '4', color: '#505050', width: '33.33%', height: '25%'},
  {content: '5',color: '#505050', width: '33.33%', height: '25%'},
  {content: '6',color: '#505050', width: '33.33%', height: '25%'},
  {content: '7', color: '#505050', width: '33.33%', height: '25%'},
  {content: '8',color: '#505050', width: '33.33%', height: '25%'},
  {content: '9',color: '#505050', width: '33.33%', height: '25%'},
  {content: '.', color: '#505050', width: '33.33%', height: '25%'},
  {content: '0',color: '#505050', width: '33.33%', height: '25%'},
  {content: '=',color: '#505050', width: '33.33%', height: '25%'}
];
const portraitOperations = [
  { content: 'DEL', color: '#FF9500', width: '100%'},
  { content: '+', color: '#FF9500', width: '100%'},
  { content: '-', width: '25%', color: '#FF9500', width: '100%'},
  { content: '*', width: '25%', color: '#FF9500', width: '100%'},
  {content: '/', color: '#FF9500', width: '100%'}
];
const landscapeOperations = [
  {content: '2√x', color: '#333333', width: '33.33%', height: '25%'},
  {content: 'x!',color: '#333333', width: '33.33%', height: '25%'},
  {content: 'e^x',color: '#333333', width: '33.33%', height: '25%'},
  {content: '10^x',color: '#333333', width: '33.33%', height: '25%'},
  {content: 'ln', color: '#333333', width: '33.33%', height: '25%'},
  {content: 'log10',color: '#333333', width: '33.33%', height: '25%'},
  {content: 'e',color: '#333333', width: '33.33%', height: '25%'},
  {content: 'x^2', color: '#333333', width: '33.33%', height: '25%'},
  {content: 'pi',color: '#333333', width: '33.33%', height: '25%'},
  {content: 'x^x',color: '#333333', width: '33.33%', height: '25%'},
  {content: '+/-', color: '#333333', width: '33.33%', height: '25%'},
  {content: '%',color: '#333333', width: '33.33%', height: '25%'}
];

export default class App extends Component {
  componentDidMount() {
        SplashScreen.hide();
    }

  constructor() {
    super()

    this.operations = ['DEL','+','-','*','/'];

    const isPortrait = () => {
      const dim = Dimensions.get('window');
      return dim.height > dim.width;
    };

    this.state = {
      resultText: "",
      calculationText: "",
      orientation: isPortrait() ? 'PORTRAIT' : 'LANDSCAPE'
    };

    Dimensions.addEventListener('change', () => {
      this.setState({
        orientation: isPortrait() ? 'PORTRAIT' : 'LANDSCAPE'
      });
    });
  }

  calculateResult(){
    const text = this.state.resultText
    console.log(text, mexp.eval(text))
    this.setState({
      calculationText: mexp.eval(text)
    })
  }

  buttonPressed(text){
    if(text == '='){
      return this.validate() && this.calculateResult()
    }

    this.setState({
        resultText: this.state.resultText + text
      })
  }
  validate(){
    const text = this.state.resultText
    if(text === ""){
      this.setState({
        calculationText: ""
      })
      return false
    }
    if(text.includes('!!')){
      return false
    }
    if(text.includes('ln')){
      if(text.includes('ln0') || text.includes('ln1') || text.includes('ln2') ||
       text.includes('ln3') || text.includes('ln4') || text.includes('ln5') ||
       text.includes('ln6') || text.includes('ln7') || text.includes('ln8') ||
       text.includes('ln9')){
        return true
      }
      else{
        return false
      }
    }
    if(text.includes('log')){
      if(text.includes('log0') || text.includes('log1') || text.includes('log2') ||
       text.includes('log3') || text.includes('log4') || text.includes('log5') ||
       text.includes('log6') || text.includes('log7') || text.includes('log8') ||
       text.includes('log9')){
        return true
      }
      else{
        return false
      }
    }
    if(text.includes('(')){
      if(text.includes(')')){
        return true
      }
      return false
    }
    switch (text.slice(-1)) {
      case '+':
      case '-':
      case '*':
      case '/':
      case '(':
      case '^':
      case 'g':
      case 'n':
        return false
      break;
    }
    switch (text.charAt(0)) {
      case '+':
      case '*':
      case '/':
      case '√':
        return false
      break;

    }
    return true
  }

  factorial(number){
    if(number < 21){
      if(number == 0 || number == 1){
        return 1
      }
      else{
        let fact = 1;
        while(number > 1){
          fact = fact * number
          number = number - 1
        }
        return fact
      }
    }
  }

  operate(operation){
    switch (operation) {
      case 'DEL':
        this.setState({
            resultText: ''
          })
        break;
      case '+':
      case '-':
      case '*':
      case '/':
        const lastChar = this.state.resultText.split('').pop();
        if(this.state.resultText == "" || lastChar == "^" ||
        lastChar == "g" || lastChar == "n" || this.operations.indexOf(lastChar) > 0){
          return;
        }
        this.setState({
          resultText: this.state.resultText + operation
        })
      break;

    }
  }
  operateLand(operation){
    let ev = 0;
    const text = this.state.resultText
    switch (operation) {
      case 'pi':
      case 'e':
      case 'ln':
          this.setState({
              resultText: this.state.resultText + operation
          })
        break;
      case 'e^x':
      case '10^x':
          this.setState({
              resultText: this.state.resultText + operation.slice(0, -1)
            })
        break;
      case 'x^2':
        if(text != ''){
            if(Number.isInteger(parseInt(text.slice(-1)))){
              this.setState({
                  resultText: this.state.resultText + operation.slice(1,3)
                })
              }
            }
        break;
      case 'x^x':
        if(text != ''){
          if(Number.isInteger(parseInt(text.slice(-1)))){
            this.setState({
              resultText: this.state.resultText + operation.slice(1,2)
            })
          }
        }
        break;
      case 'log10':
          this.setState({
              resultText: this.state.resultText + operation.slice(0,-2)
            })
        break;
      case 'x!':
          if(text != ''){
            if(Number.isInteger(parseInt(text.slice(-1)))){
              this.setState({
                  resultText: this.state.resultText + operation.slice(-1)
                })
              }
            }
        break;
      case '2√x':
        if(this.validate()){
          ev = mexp.eval(text);
          this.setState({
            resultText: "" + Math.sqrt(ev) + "",
            calculationText: "√" + ev
          });
        }
        break;
      case '+/-':
        if(this.validate()){
          ev = mexp.eval(text);
          this.setState({
            resultText: "-(" + ev + ")"
          })
        }
        break;
      case '%':
        if(this.validate()){
          ev = eval(text);
          this.setState({
            resultText: "" + ev/100 + ""
          })
        }
      break;
    }
  }

  render() {
      return (
          <View style={styles.container}>
            <View style={styles.result}>
              <Text style={styles.resultText}>{this.state.resultText}</Text>
            </View>
            <View style={styles.calculation}>
              <Text style={styles.calculationText}>{this.state.calculationText}</Text>
            </View>
            <View style={styles.buttons}>
              {this.state.orientation == 'LANDSCAPE' ?
                  <View style={styles.landscapeOperations}>
                    {
                      landscapeOperations.map((el) => {
                                      return (
                                          <NumberTouchableOpacity key={el.content} content={el.content} color={el.color} width={el.width} height={el.height}
                                                               onBtnPress={this.operateLand.bind(this, el.content)}/>
                                      );
                                  })
                    }
                  </View> : null
                }
              <View style={styles.numbers}>
                {
                  portraitNumbers.map((el) => {
                                  return (
                                      <NumberTouchableOpacity key={el.content} content={el.content} color={el.color} width={el.width} height={el.height}
                                                           onBtnPress={this.buttonPressed.bind(this, el.content)}/>
                                  );
                              })
                }
              </View>
              <View style={styles.operations}>
                {
                  portraitOperations.map((el) => {
                                  return (
                                      <OperationTouchableOpacity key={el.content} content={el.content} color={el.color} width={el.width}
                                                           onBtnPress={this.operate.bind(this, el.content)}/>
                                  );
                              })
                }
              </View>
            </View>
          </View>
        );
    }
}
